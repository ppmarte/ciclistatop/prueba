<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use  yii\widgets\ListView;
$titulo = 'Ciclistas de la vuelta España';
?>


<div class="jumbotron ">
         <div class="body-content">
                 
        <?=   ListView::widget([
            'dataProvider' => $dataProvider,
           'itemView' => '_ciclistas',
            'layout'=>"{summary}\n{pager}\n{items}",
            
        ]);
?>
        
               </div>
      </div>
        
 